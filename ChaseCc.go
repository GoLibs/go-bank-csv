package bankcsv

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/akaito/go-bank-interop"
)

const (
	chaseCcColType = iota
	chaseCcColTransDate
	chaseCcColPostDate
	chaseCcColDescription
	chaseCcColAmount

	chaseCcActFeatures = 0

	chaseCcTrnFeatures = TrnFeature_PostDate |
		TrnFeature_Description |
		TrnFeature_Amount

	chaseCcDateFormat = "1/2/2006"
)

//===========================================================================
func addChaseCcTransaction(trns *[]bankop.TxnReadWriter, trnRec []string) error {
	amountF, err := strconv.ParseFloat(trnRec[chaseCcColAmount], 64)
	if err != nil {
		return err
	}

	postDate, err := time.Parse(chaseCcDateFormat, trnRec[chaseCcColPostDate])
	if err != nil {
		return err
	}

	trn := transaction{
		Features:    chaseCcTrnFeatures,
		amount:      int64(amountF * 100.0),
		description: trnRec[chaseCcColDescription],
		postDate:    postDate,
	}

	*trns = append(*trns, &trn)
	return nil
}

//===========================================================================
func UnmarshalChaseCcCsv(reader io.Reader) ([]Account, []bankop.TxnReadWriter, error) {
	csvReader := csv.NewReader(reader)

	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, nil, err
	}

	chaseCcHeader := []string{
		"Type",
		"Trans Date",
		"Post Date",
		"Description",
		"Amount",
	}

	// validate header
	if len(records[0]) != len(chaseCcHeader) {
		return nil, nil, fmt.Errorf("Unexpected header format. Want {%s}.", chaseCcHeader)
	}
	for i, v := range chaseCcHeader {
		if v != records[0][i] {
			return nil, nil, fmt.Errorf(
				"Header column {%s} at index {%d} unexpected.  Wanted {%s}.",
				records[0][i], i, v)
		}
	}

	// build bankcsv common representation
	var acts []Account
	var trns []bankop.TxnReadWriter
	for _, trnRec := range records[1:] {
		if err := addChaseCcTransaction(&trns, trnRec); err != nil {
			return nil, nil, err
		}
	}

	return acts, trns, nil
}
