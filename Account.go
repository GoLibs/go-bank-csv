package bankcsv

const (
	ActFeature_Id = 1 << iota
	ActFeature_BankId
	ActFeature_Type
)

type Account struct {
	Features uint
	Id       string
	BankId   string
	Type     string
}
