package bankcsv

import (
	"time"
)

const (
	TrnFeature_Id = 1 << iota
	TrnFeature_AccountId
	TrnFeature_Amount
	TrnFeature_BalancePost // account balance after the transaction
	TrnFeature_Description
	TrnFeature_PostDate
)

type transaction struct {
	Features    uint
	id          string
	AccountId   string
	amount      int64
	BalancePost int64
	description string
	postDate    time.Time
}

//===========================================================================
// github.com/akaito/chb-budget-interop TxnReader interface
//===========================================================================

//===========================================================================
func (t transaction) Amount() int64 {
	return t.amount
}

//===========================================================================
func (t transaction) Description() string {
	return t.description
}

//===========================================================================
func (t transaction) Id() string {
	return t.id
}

//===========================================================================
func (t transaction) PostDate() time.Time {
	return t.postDate
}

//===========================================================================
func (t *transaction) SetAmount(amt int64) {
	t.amount = amt
}

//===========================================================================
func (t *transaction) SetDescription(desc string) {
	t.description = desc
}
