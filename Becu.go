package bankcsv

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/akaito/go-bank-interop"
)

const (
	becuColDate = iota
	becuColNumber
	becuColDescription
	becuColDebit
	becuColCredit

	becuActFeatures = 0

	becuTrnFeatures = TrnFeature_Amount |
		TrnFeature_Description |
		TrnFeature_PostDate

	becuDateFormat = "1/2/2006"
)

//===========================================================================
func addBecuTransaction(trns *[]bankop.TxnReadWriter, trnRec []string) error {
	amount := int64(0)

	if len(trnRec[becuColDebit]) != 0 {
		debitF, err := strconv.ParseFloat(trnRec[becuColDebit], 64)
		if err != nil {
			return err
		}
		amount += int64(debitF * 100.0)
	}

	if len(trnRec[becuColCredit]) != 0 {
		creditF, err := strconv.ParseFloat(trnRec[becuColCredit], 64)
		if err != nil {
			return err
		}
		amount += int64(creditF * 100.0)
	}

	postDate, err := time.Parse(becuDateFormat, trnRec[becuColDate])
	if err != nil {
		return err
	}

	trn := transaction{
		Features:    becuTrnFeatures,
		amount:      amount,
		description: trnRec[becuColDescription],
		postDate:    postDate,
	}

	*trns = append(*trns, &trn)
	return nil
}

//===========================================================================
func UnmarshalBecuCsv(reader io.Reader) ([]Account, []bankop.TxnReadWriter, error) {
	csvReader := csv.NewReader(reader)

	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, nil, err
	}

	becuHeader := []string{
		"Date",
		"No.",
		"Description",
		"Debit",
		"Credit",
	}

	// validate header
	if len(records[0]) != len(becuHeader) {
		return nil, nil, fmt.Errorf("Unexpected header format. Want {%s}.", becuHeader)
	}
	for i, v := range becuHeader {
		if v != records[0][i] {
			return nil, nil, fmt.Errorf(
				"Header column {%s} at index {%d} unexpected.  Wanted {%s}.",
				records[0][i], i, v)
		}
	}

	// build bankcsv common representation
	var acts []Account
	var trns []bankop.TxnReadWriter
	for _, trnRec := range records[1:] {
		if err := addBecuTransaction(&trns, trnRec); err != nil {
			return nil, nil, err
		}
	}

	return acts, trns, nil
}
