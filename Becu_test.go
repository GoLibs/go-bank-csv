package bankcsv

import (
	"os"
	"os/user"
	"testing"
)

const (
	becuFilepath = "/tmp/becu.csv"
)

func TestUnmarshalBecu(t *testing.T) {
	usr, err := user.Current()
	if err != nil {
		t.Error(err)
	}
	filepath := usr.HomeDir + becuFilepath

	file, err := os.Open(filepath)
	if err != nil {
		t.Error(err)
	}
	defer file.Close()

	acts, trns, err := UnmarshalBecuCsv(file)
	if err != nil {
		t.Error(err)
	}

	for _, a := range acts {
		t.Log(a)
	}
	for _, trn := range trns {
		t.Log(trn)
	}
	t.Logf("Accounts Found: {%d}\n", len(acts))
	t.Logf("Transactions Found: {%d}\n", len(trns))
}
