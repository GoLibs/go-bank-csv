package bankcsv

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"github.com/akaito/go-bank-interop"
)

const (
	co360ColBankId = iota
	co360ColAccountNumber
	co360ColAccountType
	co360ColBalance // after transaction
	co360ColStatementStartDate
	co360ColStatementEndDate
	co360ColTransactionType
	co360ColTransactionDate
	co360ColTransactionAmount
	co360ColTransactionId
	co360ColTransactionDescription

	co360ActFeatures = ActFeature_Id |
		ActFeature_BankId |
		ActFeature_Type

	co360TrnFeatures = TrnFeature_Id |
		TrnFeature_AccountId |
		TrnFeature_Amount |
		TrnFeature_BalancePost |
		TrnFeature_Description |
		TrnFeature_PostDate

	co360DateFormat = "2006-01-02"
)

//===========================================================================
func addCo360Transaction(trns *[]bankop.TxnReadWriter, trnRec []string) error {
	amountF, err := strconv.ParseFloat(trnRec[co360ColTransactionAmount], 64)
	if err != nil {
		return err
	}

	balancePostF, err := strconv.ParseFloat(trnRec[co360ColBalance], 64)
	if err != nil {
		return err
	}

	postDate, err := time.Parse(co360DateFormat, trnRec[co360ColTransactionDate])
	if err != nil {
		return err
	}

	trn := transaction{
		Features:    co360TrnFeatures,
		id:          trnRec[co360ColTransactionId],
		AccountId:   trnRec[co360ColAccountNumber],
		amount:      int64(amountF * 100.0),
		BalancePost: int64(balancePostF * 100.0),
		description: trnRec[co360ColTransactionDescription],
		postDate:    postDate,
	}

	*trns = append(*trns, &trn)
	return nil
}

//===========================================================================
func tryAddCo360Account(acts *[]Account, trnRec []string) {
	for _, act := range *acts {
		if act.BankId == trnRec[co360ColBankId] && act.Id == trnRec[co360ColAccountNumber] {
			return
		}
	}

	act := Account{
		Features: co360ActFeatures,
		Id:       trnRec[co360ColAccountNumber],
		BankId:   trnRec[co360ColBankId],
		Type:     trnRec[co360ColAccountType],
	}
	*acts = append(*acts, act)
}

//===========================================================================
func UnmarshalCo360Csv(reader io.Reader) ([]Account, []bankop.TxnReadWriter, error) {
	csvReader := csv.NewReader(reader)

	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, nil, err
	}

	co360Header := []string{
		"BANK ID",
		"Account Number",
		"Account Type",
		"Balance",
		"Start Date",
		"End Date",
		"Transaction Type",
		"Transaction Date",
		"Transaction Amount",
		"Transaction ID",
		" Transaction Description", // leading space is intentional
	}

	// validate header
	if len(records[0]) != len(co360Header) {
		return nil, nil, fmt.Errorf("Unexpected header format. Want {%s}.", co360Header)
	}
	for i, v := range co360Header {
		if v != records[0][i] {
			return nil, nil, fmt.Errorf(
				"Header column {%s} at index {%d} unexpected.  Wanted {%s}.",
				records[0][i], i, v)
		}
	}

	// build bankcsv common representation
	var acts []Account
	var trns []bankop.TxnReadWriter
	for _, trnRec := range records[1:] {
		tryAddCo360Account(&acts, trnRec)
		if err := addCo360Transaction(&trns, trnRec); err != nil {
			return nil, nil, err
		}
	}

	return acts, trns, nil
}
